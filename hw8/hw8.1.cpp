#include <iostream>
#include <vector>

using namespace std;

vector<int> calculateMostCoins(int &pennies, int &nickels, int &dimes, int &quarters, int &price)
{
    vector<int> coinage;
    vector<vector<int>> subProblems; //index is price, value is remaining coins
    vector<int> startCoinage;

    startCoinage.push_back(pennies);
    startCoinage.push_back(nickels);
    startCoinage.push_back(dimes);
    startCoinage.push_back(quarters);
    subProblems.push_back(startCoinage); //we still have all our coins at price 0

    for (int i = 1; i <= price; i++)
    {
        if (!subProblems[i - 1].empty() && subProblems[i - 1][0] > 0)
        {
            //if we still got pennies use them
            vector<int> newCoinage(subProblems[i - 1]);
            newCoinage[0] = newCoinage[0] - 1;
            subProblems.push_back(newCoinage);
        }
        else if (i - 5 >= 0 && !subProblems[i - 5].empty() && subProblems[i - 5][1] > 0)
        {
            //if we still got nickels use them
            vector<int> newCoinage(subProblems[i - 5]);
            newCoinage[1] = newCoinage[1] - 1;
            subProblems.push_back(newCoinage);
        }
        else if (i - 10 >= 0 && !subProblems[i - 10].empty() && subProblems[i - 10][2] > 0)
        {
            //if we still got dimes use them
            vector<int> newCoinage(subProblems[i - 10]);
            newCoinage[2] = newCoinage[2] - 1;
            subProblems.push_back(newCoinage);
        }
        else if (i - 25 >= 0 && !subProblems[i - 25].empty() && subProblems[i - 25][3] > 0)
        {
            //if we still got quarters use them
            vector<int> newCoinage(subProblems[i - 25]);
            newCoinage[3] = newCoinage[3] - 1;
            subProblems.push_back(newCoinage);
        }
        else
        {
            //No coinage possible
            subProblems.push_back(vector<int>());
        }

        if (i == price)
        {
            //we did it bois
            if (subProblems[i].empty())
            {
                break;
            }
            coinage.push_back(pennies - subProblems[i][0]);
            coinage.push_back(nickels - subProblems[i][1]);
            coinage.push_back(dimes - subProblems[i][2]);
            coinage.push_back(quarters - subProblems[i][3]);

            pennies = subProblems[i][0];
            nickels = subProblems[i][1];
            dimes = subProblems[i][2];
            quarters = subProblems[i][3];
        }
    }

    return coinage;
}

void printCoins(vector<int> &coinage)
{
    if (coinage.size() == 4)
    {
        printf("-%00i %00i %00i %00i\n", coinage[0], coinage[1], coinage[2], coinage[3]);
    }
    else
    {
        printf("ERROR: Unable to pay!\n");
    }
    printf("-----------\n");
}

void printCoins(int &pennies, int &nickels, int &dimes, int &quarters)
{

    printf(" %00i %00i %00i %00i\n", pennies, nickels, dimes, quarters);
}

int main(int argc, char *argv[])
{
    printf("How many pennies, nickels dimes and quarters do you have:\n");
    int pennies = 0;
    int nickels = 0;
    int dimes = 0;
    int quarters = 0;

    //assume perfect input
    cin >> pennies;
    cin.ignore();
    cin >> nickels;
    cin.ignore();
    cin >> dimes;
    cin.ignore();
    cin >> quarters;
    cin.clear();
    cin.ignore(INT32_MAX, '\n');

    printCoins(pennies, nickels, dimes, quarters);
    while (true)
    {
        printf("Enter your price:\n");
        int price = 0;
        cin >> price;
        cin.ignore(INT32_MAX, '\n');

        vector<int> coinage = calculateMostCoins(pennies, nickels, dimes, quarters, price);
        printCoins(coinage);
        printCoins(pennies, nickels, dimes, quarters);
    }
    return 0;
}