#include <iostream>
#include <vector>
#include <queue>

using namespace std;

vector<char> parseDNA(string &dnaString)
{
    vector<char> dna;
    for (char c : dnaString)
    {
        switch (c)
        {
        case 'A':
        case 'G':
        case 'C':
        case 'T':
            dna.push_back(c);
            break;
        case 'a':
            dna.push_back('A');
            break;
        case 'g':
            dna.push_back('G');
            break;
        case 'c':
            dna.push_back('C');
            break;
        case 't':
            dna.push_back('T');
            break;
        case ' ':
        case '\t':
        case '\n':
            break;
        default:
            printf("ERROR: Could not read DNA sequence");
            return vector<char>();
        }
    }
    return dna;
}

void printDNA(vector<char> &dna)
{
    for (char c : dna)
    {
        printf("%c ", c);
    }
    printf("\n");
}

int matches(char c1, char c2)
{
    switch (c1)
    {
    case 'A':
        switch (c2)
        {
        case 'A':
            return 10;
        case 'T':
            return 0;
        case 'C':
            return 7;
        case 'G':
            return 10;
        }
    case 'G':
        switch (c2)
        {
        case 'A':
            return 10;
        case 'T':
            return 7;
        case 'C':
            return 0;
        case 'G':
            return 10;
        }
    case 'C':
        switch (c2)
        {
        case 'A':
            return 7;
        case 'T':
            return 6;
        case 'C':
            return 6;
        case 'G':
            return 0;
        }
    case 'T':
        switch (c2)
        {
        case 'A':
            return 0;
        case 'T':
            return 6;
        case 'C':
            return 6;
        case 'G':
            return 7;
        }
    }
    return -1;
}

void matchDNAWithSpacers(vector<char> &dna1, vector<char> &dna2)
{
    vector<vector<int>> solutions; //we create a 2d array the y axis is the characters of dna1 and the x axis is the characters of dna2. The value is the number of mismatches
    vector<vector<int>> solutionsPath;
    vector<int> emptyRow(dna2.size(), 0);
    solutions.insert(solutions.begin(), dna1.size(), emptyRow);
    solutionsPath.insert(solutionsPath.begin(), dna1.size(), emptyRow);
    for (int i = 0; i < dna1.size(); i++)
    {
        for (int j = 0; j < dna2.size(); j++)
        {
            int space1 = 4;
            int space2 = 4;
            int nospace = matches(dna1[i], dna2[j]);
            if (i > 0)
            {
                space1 += solutions[i - 1][j];
            }
            if (j > 0)
            {
                space2 += solutions[i][j - 1];
            }
            if (i > 0 && j > 0 && nospace > -1)
            {
                nospace += solutions[i - 1][j - 1];
            }

            if (nospace > -1 && (nospace <= space1 || space1 == -1) && (nospace <= space2 || space2 == -1))
            {
                solutions[i][j] = nospace;
                solutionsPath[i][j] = 0;
            }
            else if (space1 > -1 && (space1 < space2 || space2 == -1))
            {
                solutions[i][j] = space1;
                solutionsPath[i][j] = -1;
            }
            else
            {
                solutions[i][j] = space2;
                solutionsPath[i][j] = 1;
            }
        }
    }

    // For Debugging
    // for (int j = solutions.size() - 1; j >= 0; j--)
    // {
    //     for (int i : solutions[j])
    //     {
    //         printf("%i ", i);
    //     }
    //     printf("\n");
    // }
    // printf("\n");

    // for (int j = solutionsPath.size() - 1; j >= 0; j--)
    // {
    //     for (int i : solutionsPath[j])
    //     {
    //         switch (i)
    //         {
    //         case 0:
    //             printf("0 ");
    //             break;
    //         case -1:
    //             printf("V ");
    //             break;
    //         case 1:
    //             printf("< ");
    //             break;
    //         }
    //     }
    //     printf("\n");
    // }
    // printf("\n");

    //rebuild the sequences with spaces
    int i = dna1.size() - 1;
    int j = dna2.size() - 1;
    while (i >= 0 && j >= 0)
    {
        switch (solutionsPath[i][j])
        {
        case 0:
            i--;
            j--;
            break;
        case -1:
            dna2.insert(dna2.begin() + j + 1, '_');
            i--;
            break;
        case 1:
            dna1.insert(dna1.begin() + i + 1, '_');
            j--;
            break;
        }
    }
    while (dna1.size() < dna2.size())
    {
        dna1.insert(dna1.begin(), '_');
    }
    while (dna2.size() < dna1.size())
    {
        dna2.insert(dna2.begin(), '_');
    }
}

int main(int argc, char *argv[])
{
    printf("Please enter two DNA sequences:\n");
    string dnaSequence;
    vector<char> dna1;
    vector<char> dna2;
    while (dna1.empty())
    {
        getline(cin, dnaSequence);
        dna1 = parseDNA(dnaSequence);
    }
    dnaSequence.clear();
    while (dna2.empty())
    {
        getline(cin, dnaSequence);
        dna2 = parseDNA(dnaSequence);
    }

    matchDNAWithSpacers(dna1, dna2);
    printf("\n");
    printDNA(dna1);
    printDNA(dna2);
    return 0;
}