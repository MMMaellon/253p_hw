#include <iostream>
#include <string.h>
#include <sstream>
#include <fstream>
#include <stdio.h>
#include <vector>
#include <limits>

#define STRMAX 100
#define LIBMAX 1024

using namespace std;

struct Song
{
    char title[STRMAX];
    char artist[STRMAX];
    int year_published;
};

struct MusicLibrary
{
    string name;
    Song songs[LIBMAX];
    int current_number_of_songs;
};

//Commands
#define INSERT 'I'
#define PRINT 'P'
#define DELETE 'D'
#define LOOKUP 'L'
#define SAVE 'Q'

char read_command(const char *currentName)
{
    printf("%s Command: ", currentName);
    char cmd;
    string response = "";
    //use getline to prevent lingering newline characters
    getline(cin, response);

    int counter = 0;
    while (true)
    {
        counter = (counter + 1) % 3;
        if (counter == 0)
        {
            printf("Valid Commands: \n\tI - insert\n\tP - print\n\tD - delete\n\tL - lookup\n\tQ - save and quit\n");
        }
        if (response.length() == 1)
        {
            cmd = toupper(response.at(0));
            if (cmd == INSERT || cmd == PRINT || cmd == DELETE || cmd == LOOKUP || cmd == SAVE)
            {
                break;
            }
        }
        printf("Error! Invalid Command: %s\n", response.c_str());
        getline(cin, response);
    }
    return cmd;
}

void stringPrompt(const char *prompt, char *result, int maxlimit = STRMAX, int minlimit = 1)
{
    printf("%s\n", prompt);
    string response;
    getline(cin, response);
    while (response.length() < minlimit || response.length() > maxlimit)
    {
        printf("Error: Invalid String. Enter a String Between %d and %d Characters\n%s\n", minlimit, maxlimit, prompt);
        getline(cin, response);
    }
    strcpy(result, response.c_str());
}

int intPrompt(const char *prompt, int maxlimit = 2020, int minlimit = 0)
{
    printf("%s\n", prompt);
    int num = minlimit;
    bool error = !(cin >> num);
    if (error)
    {
        cin.clear();
    }
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    while (error || num < minlimit || num > maxlimit)
    {
        printf("Error: Invalid Number. Enter a number Between %d and %d\n%s\n", minlimit, maxlimit, prompt);
        error = !(cin >> num);
        if (error)
        {
            cin.clear();
        }
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }
    return num;
}

int stringCompare(const char s1[], const char s2[])
{
    int equal;
    int s1Len = strlen(s1);
    int s2Len = strlen(s2);
    int length = s1Len;
    if (s1Len < s2Len)
    {
        equal = -1;
    }
    else if (s1Len > s2Len)
    {
        length = s2Len;
        equal = 1;
    }
    else
    {
        equal = 0;
    }
    for (int i = 0; i < length; i++)
    {
        int compare = toupper(s1[i]) - toupper(s2[i]);
        if (compare < 0)
        {
            equal = -1;
            break;
        }
        else if (compare > 0)
        {
            equal = 1;
            break;
        }
    }
    return equal;
}

int find_index_of_song_with_name(MusicLibrary &library, const char *title)
{
    int i = 0;
    int j = library.current_number_of_songs;
    int index = j / 2;
    while (i < j)
    {
        const char *compareTitle = library.songs[index].title;
        switch (stringCompare(title, compareTitle))
        {
        case -1:
            j = index;
            index = ((j - i) / 2) + i;
            break;
        case 1:
            i = index + 1;
            index = ((j - i) / 2) + i;
            break;
        default:
            j = i;
            break;
        }
    }
    return index;
}

void crunch_up_from_index(MusicLibrary &library, int index, int crunchAmount = 1)
{
    if (index < library.current_number_of_songs && crunchAmount <= library.current_number_of_songs)
    {
        library.current_number_of_songs -= crunchAmount;
        for (int i = index; i < library.current_number_of_songs; i++)
        {
            library.songs[i] = library.songs[i + crunchAmount];
        }
    }
}

void crunch_down_from_index(MusicLibrary &library, int index, int crunchAmount = 1)
{
    if (index >= 0 && crunchAmount <= LIBMAX - library.current_number_of_songs)
    {
        library.current_number_of_songs += crunchAmount;
        for (int i = library.current_number_of_songs; i >= index; i--)
        {
            library.songs[i + crunchAmount] = library.songs[i];
        }
    }
}

void add_song_to_MusicLibrary(MusicLibrary &library, char title[], char artist[], int year_published)
{
    int insertIndex = find_index_of_song_with_name(library, title);

    Song song;
    strcpy(song.title, title);
    strcpy(song.artist, artist);
    song.year_published = year_published;

    crunch_down_from_index(library, insertIndex);
    library.songs[insertIndex] = song;
}

void print_MusicLibrary(MusicLibrary &library)
{
    printf("Music Library: %s\n", library.name.c_str());
    for (int i = 0; i < library.current_number_of_songs; i++)
    {
        Song s = library.songs[i];
        printf("\t%d) Title: %s\tArtist: %s\tYear: %d\n", i + 1, s.title, s.artist, s.year_published);
    }
    if (library.current_number_of_songs == 0)
    {
        printf("\tempty library\n");
    }
}

void remove_song_from_MusicLibrary_by_name(MusicLibrary &library, const char *songName)
{
    int start, end; //Range of all values that have matching titles
    start = find_index_of_song_with_name(library, songName);
    end = start;
    if(start < library.current_number_of_songs)
    {
        while (start - 1 >= 0 && stringCompare(songName, library.songs[start - 1].title) == 0)
        {
            start--;
        }
        while (end < library.current_number_of_songs && stringCompare(songName, library.songs[end].title) == 0)
        {
            end++;
        }
    }
    int deleteNum = end - start;
    crunch_up_from_index(library, start, deleteNum);
    printf("%d Songs Deleted\n", deleteNum);
}

void load_MusicLibrary(MusicLibrary &library)
{
    printf("Opening file %s...\n", library.name.c_str());
    ifstream file(library.name);
    if (file)
    {
        if (file.eof())
        {
            printf("Warning: File is empty\n");
        }
        while (!file.eof())
        {
            Song s;
            string title, artist, year_published;
            if (!getline(file, title))
            {
                //reached end of file normally
                break;
            }
            else
            {
                if (getline(file, artist) && getline(file, year_published))
                {
                    istringstream ss(year_published);
                    if (ss >> s.year_published)
                    {
                        strcpy(s.title, title.c_str());
                        strcpy(s.artist, artist.c_str());
                        library.songs[library.current_number_of_songs++] = s;
                    }
                    else
                    {
                        printf("Error: Improperly formatted file\n");
                        break;
                    }
                }
                else
                {
                    printf("Error: Improperly formatted file\n");
                    break;
                }
            }
        }
    }
    else
    {
        printf("Could not open file %s\n", library.name.c_str());
    }
    file.close();
}

void store_MusicLibrary(MusicLibrary &library)
{
    printf("Saving file %s...\n", library.name.c_str());
    ofstream file(library.name);
    if (file)
    {
        for (int i = 0; i < library.current_number_of_songs; i++)
        {
            Song s = library.songs[i];
            file << s.title << endl;
            file << s.artist << endl;
            file << s.year_published << endl;
        }
        if (library.current_number_of_songs == 0)
        {
            printf("\tempty file written\n");
        }
    }
    else
    {
        printf("Could not save file %s\n", library.name.c_str());
        printf("Would you still like to quit? [y/n]\n");
        char response = 'a';
        while (response != 'y' && response != 'Y' && response != 'n' && response != 'N')
        {
            if (!(cin >> response))
            {
                cin.clear();
            }
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            if (response == 'n' || response == 'N')
            {
                return;
            }
            else if (response != 'y' || response != 'Y')
            {
                printf("Please enter either 'y' or 'n'\n");
            }
        }
    }
    file.close();
    printf("Goodbye!\n");
    exit(0);
}

MusicLibrary initLibrary(const char *name)
{
    MusicLibrary m;
    m.name = name;
    m.current_number_of_songs = 0;
    return m;
}

void evaluate_command(MusicLibrary &library, char command)
{
    switch (command)
    {
    case (INSERT):
    {
        char title[STRMAX], artist[STRMAX];
        stringPrompt("Song Title: ", title);
        stringPrompt("Song Artist: ", artist);
        int year_published = intPrompt("Song Year: ");
        add_song_to_MusicLibrary(library, title, artist, year_published);
        break;
    }
    case (PRINT):
        print_MusicLibrary(library);
        break;
    case (DELETE):
    {
        char title[STRMAX];
        stringPrompt("Song Title (Warning! All matching songs will be deleted): ", title);
        remove_song_from_MusicLibrary_by_name(library, title);
        break;
    }
    case (LOOKUP):
    {
        char name[STRMAX];
        stringPrompt("Please Enter The Song Name: \n", name);
        int index = find_index_of_song_with_name(library, name);
        if (index < library.current_number_of_songs && stringCompare(library.songs[index].title, name) == 0)
        {
            Song s = library.songs[index];
            printf("%d) Title: %s\tArtist: %s\tYear: %d\n", index + 1, s.title, s.artist, s.year_published);
        } else
        {
            printf("Couldn't find %s\n", name);
        }
        break;
    }
    case (SAVE):
        store_MusicLibrary(library);
        break;
    default:
        printf("Error! Could Not Perform Command: %c\n", command);
    }
}

int main(int argc, char *argv[])
{
    MusicLibrary library = initLibrary("myMusic");

    if (argc > 1)
    {
        library.name = argv[1];
        load_MusicLibrary(library);
    }
    else
    {
        printf("No filename given. Creating new Music Library called myMusic\n");
    }

    //Main Program Loop
    char command = 0;
    while (command != SAVE)
    {
        command = read_command(library.name.c_str());
        evaluate_command(library, command);
    }

    return 0;
}