#include <string>
#include <unordered_map>
#include <iostream>
#include <sstream>
#include <vector>

using namespace std;

class Person
{
private:
    string name;
    Person *parent1;
    Person *parent2;
    double chance1, chance2;
    double genoCache, phenoCache;

public:
    Person(string p_name, Person *p_parent1 = nullptr, Person *p_parent2 = nullptr) : name(p_name), parent1(p_parent1), parent2(p_parent2), chance1(-1.0), chance2(-1.0), genoCache(-1.0), phenoCache(-1.0) {}
    Person(string p_name, double p_chance1, double p_chance2) : name(p_name), parent1(nullptr), parent2(nullptr), chance1(p_chance1), chance2(p_chance2), genoCache(-1.0), phenoCache(-1.0) {}
    double calcGenoChance()
    {
        if (genoCache < 0)
        {
            double c1 = chance1;
            double c2 = chance2;
            if ((chance1 < 0 || chance2 < 0) && (parent1 != nullptr && parent2 != nullptr))
            {
                c1 = parent1->calcGenoChance();
                c2 = parent2->calcGenoChance();
            }
            genoCache = (c1 + c2) / 2.0;
        }
        return genoCache;
    }
    double calcPhenoChance()
    {
        if (phenoCache < 0)
        {
            double c1 = chance1;
            double c2 = chance2;
            if ((chance1 < 0 || chance2 < 0) && (parent1 != nullptr && parent2 != nullptr))
            {
                c1 = parent1->calcGenoChance();
                c2 = parent2->calcGenoChance();
            }
            phenoCache = 1.0 - ((1.0 - c1) * (1.0 - c2));
        }
        return phenoCache;
    }

    string getname()
    {
        return name;
    }
};

class PeopleTree {
    vector<Person *>;
}

int
main(int argc, char *argv[])
{
    printf("Enter each person one by one. Include their genotype, phenotype, parents, or the word 'unknown'.\nValid genotypes are 'DD', 'Dd', and 'dd'.\nValid Phenotypes are 'dimples' and 'no dimples'.\nParents must be entered before their children.\nEnter a blank line to start processing the data\n");
    string line;
    getline(cin, line);
    vector<Person *> people;
    unordered_map<string, Person *> peopleIndex;
    while (!line.empty() && line.compare("\n") != 0)
    {
        stringstream ss(line);
        string name;
        ss >> name;
        string data;
        bool valid = peopleIndex.count(name) == 0;
        if (valid)
        {
            if (ss >> data)
            {
                if (data.compare("DD") == 0)
                {
                    Person *p = new Person(name, 1.0, 1.0);
                    people.push_back(p);
                    peopleIndex.insert(pair<string, Person *>(name, p));
                }
                else if (data.compare("Dd") == 0)
                {
                    Person *p = new Person(name, 1.0, 0.0);
                    people.push_back(p);
                    peopleIndex.insert(pair<string, Person *>(name, p));
                }
                else if (data.compare("dd") == 0)
                {
                    Person *p = new Person(name, 0.0, 0.0);
                    people.push_back(p);
                    peopleIndex.insert(pair<string, Person *>(name, p));
                }
                else if (data.compare("dimples") == 0)
                {
                    Person *p = new Person(name, 1.0, 0.5);
                    people.push_back(p);
                    peopleIndex.insert(pair<string, Person *>(name, p));
                }
                else if (data.compare("unknown") == 0)
                {
                    Person *p = new Person(name, 0.5, 0.5);
                    people.push_back(p);
                    peopleIndex.insert(pair<string, Person *>(name, p));
                }
                else if (data.compare("no") == 0)
                {
                    if (ss >> data)
                    {
                        if (data.compare("dimples") == 0)
                        {
                            Person *p = new Person(name, 0.0, 0.0);
                            people.push_back(p);
                            peopleIndex.insert(pair<string, Person *>(name, p));
                        }
                        else
                        {
                            valid = false;
                        }
                    }
                    else
                    {
                        valid = false;
                    }
                }
                else
                {
                    string p2;
                    if (ss >> p2)
                    {
                        unordered_map<string, Person *>::iterator parent1 = peopleIndex.find(data);
                        unordered_map<string, Person *>::iterator parent2 = peopleIndex.find(p2);
                        if (parent1 != peopleIndex.end() && parent2 != peopleIndex.end())
                        {
                            Person *p = new Person(name, parent1->second, parent2->second);
                            people.push_back(p);
                            peopleIndex.insert(pair<string, Person *>(name, p));
                        }
                        else
                        {
                            printf("ERROR: Parent Not Found\n");
                            valid = false;
                        }
                    }
                    else
                    {
                        printf("ERROR: Missing Data\n");
                        valid = false;
                    }
                }
            }
            else
            {
                printf("ERROR: Missing Data\n");
                valid = false;
            }
        }
        else
        {
            printf("ERROR: Duplicate Name\n");
        }
        if (valid)
        {
            printf("Added!\n");
        }
        else
        {
            printf("ERROR: Unable to Add Person\n");
        }
        getline(cin, line);
    }

    if (people.size() > 0)
    {
        printf("\n");
        for (int i = 0; i < people.size(); i++)
        {
            printf("%s %f\% (%f)\n", people[i]->getname().c_str(), people[i]->calcPhenoChance(), people[i]->calcGenoChance());
        }
        printf("\n");
    }
    else
    {
        printf("Nothing to print\n");
    }

    for (int i = 0; i < people.size(); i++)
    {
        delete people[i];
    }
    return 0;
}