#include <stdio.h>
#include <string>
#include <iostream>
#include <vector>
#include <limits>

using namespace std;

class Node
{
public:
    Node *left, *right;
    int val;
    int subtreeHeight;
    Node(int p_val = 0, int p_subtreeHeight = 0, Node *p_left = nullptr, Node *p_right = nullptr) : val(p_val), subtreeHeight(p_subtreeHeight), left(p_left), right(p_right){};

    void deleteSubtrees()
    {
        if (left != nullptr)
        {
            left->deleteSubtrees();
            delete left;
            left = nullptr;
        }
        if (right != nullptr)
        {
            right->deleteSubtrees();
            delete right;
            right = nullptr;
        }
    }

    void insertChild(Node *child)
    {
        if (child != nullptr)
        {
            if (child->val > val)
            {
                if (right == nullptr)
                {
                    right = child;
                }
                if (left == nullptr)
                {
                    subtreeHeight = child->subtreeHeight + 1;
                }
            }
            else
            {
                if (left == nullptr)
                {
                    left = child;
                }
                if (right == nullptr)
                {
                    subtreeHeight = child->subtreeHeight + 1;
                }
            }
        }
    }

    void rotateRight()
    {
        if (left != nullptr)
        {
            //first swap the root and the left node values
            int swapVal = left->val;
            left->val = val;
            val = swapVal;

            //then move the left child to the right child
            Node *swapChild = left;
            left = left->left;
            swapChild->left = swapChild->right;
            swapChild->right = right;
            right = swapChild;

            right->calcSubtreeHeight();
            calcSubtreeHeight();
        }
    }
    void rotateLeft()
    {
        if (right != nullptr)
        {
            //first swap the root and the left node values
            int swapVal = right->val;
            right->val = val;
            val = swapVal;

            //then move the left child to the right child
            Node *swapChild = right;
            right = right->right;
            swapChild->right = swapChild->left;
            swapChild->left = left;
            left = swapChild;

            left->calcSubtreeHeight();
            calcSubtreeHeight();
        }
    }

    void calcSubtreeHeight()
    {
        subtreeHeight = 0;
        if (left != nullptr)
        {
            subtreeHeight = left->subtreeHeight + 1;
        }
        if (right != nullptr && right->subtreeHeight + 1 > subtreeHeight)
        {
            subtreeHeight = right->subtreeHeight + 1;
        }
    }

    bool isBalanced()
    {
        int leftSize = 0;
        int rightSize = 0;
        if (left != nullptr)
        {
            leftSize = left->subtreeHeight + 1;
        }
        if (right != nullptr)
        {
            rightSize = right->subtreeHeight + 1;
        }
        return leftSize == rightSize || leftSize + 1 == rightSize || leftSize == rightSize + 1;
    }

    bool isLeftHeavy()
    {
        int leftSize = 0;
        int rightSize = 0;
        if (left != nullptr)
        {
            leftSize = left->subtreeHeight;
        }
        if (right != nullptr)
        {
            rightSize = right->subtreeHeight;
        }
        return leftSize > rightSize;
    }
};

void printPath(vector<Node *> path)
{
    for (int i = 0; i < path.size(); i++)
    {
        if (path[i] != nullptr)
        {
            printf("%i  ", path[i]->val);
        }
    }
}

class AVL
{
public:
    Node *root;

    AVL() : root(nullptr){};
    ~AVL()
    {
        if (root != nullptr)
        {
            root->deleteSubtrees();
            delete root;
            root = nullptr;
        }
    }

    vector<Node *> getFullTreeVector()
    {
        vector<Node *> result;
        if (root != nullptr)
        {
            result.push_back(root);
            for (int i = 0; i < result.size(); i++)
            {
                if (result[i]->left != nullptr)
                {
                    result.push_back(result[i]->left);
                }
                if (result[i]->right != nullptr)
                {
                    result.push_back(result[i]->right);
                }
            }
        }
        return result;
    }

    void balance(vector<Node *> path = vector<Node *>())
    {
        if (path.empty())
        {
            path = getFullTreeVector();
        }
        for (int i = path.size() - 1; i >= 0; i--)
        {
            Node *current = path[i];
            if (current != nullptr)
            {
                current->calcSubtreeHeight();
                if (!current->isBalanced())
                {

                    if (current->isLeftHeavy())
                    {
                        if (!current->left->isLeftHeavy())
                        {
                            current->left->rotateLeft();
                        }
                        current->rotateRight();
                    }
                    else
                    {
                        if (current->right->isLeftHeavy())
                        {
                            current->right->rotateRight();
                        }
                        current->rotateLeft();
                    }
                }
            }
        }
    }

    bool insert(int val)
    {
        bool success = false;
        vector<Node *> path = find(val);
        if (!path.empty())
        {
            if (path[path.size() - 1] != nullptr)
            {
                Node *leaf = new Node(val);
                path[path.size() - 1]->insertChild(leaf);
                path.push_back(leaf);
                success = true;

                //balance the tree
                balance(path);
            }
        }
        else
        {
            root = new Node(val);
            success = true;
        }
        if (success)
        {
            printf("%i  ", val);
        }
        return success;
    }

    vector<Node *> find(int val, Node *start = nullptr)
    {
        vector<Node *> path;
        Node *current = start;
        if (current == nullptr)
        {
            current = root;
        }
        while (current != nullptr)
        {
            if (start == nullptr)
            {
                printf("%i  ", current->val);
            }
            path.push_back(current);
            if (current->val > val)
            {
                current = current->left;
            }
            else if (current->val < val)
            {
                current = current->right;
            }
            else
            {
                break;
            }
        }
        return path;
    }

    bool erase(int val)
    {
        bool success = false;
        vector<Node *> path = find(val);
        if (!path.empty() && path[path.size() - 1] != nullptr && path[path.size() - 1]->val == val)
        {
            Node *toKill = path[path.size() - 1];
            Node *parent = nullptr;
            if (path.size() > 1)
            {
                parent = path[path.size() - 2];
            }

            if (toKill->left != nullptr)
            {
                Node *leftChild = toKill->left;
                Node *leftParent = toKill;
                while (leftChild->right != nullptr)
                {
                    path.push_back(leftChild);
                    leftParent = leftChild;
                    leftChild = leftChild->right;
                }
                toKill->val = leftChild->val;
                if (leftParent->left == leftChild)
                {
                    leftParent->left = leftChild->left;
                }
                else
                {
                    leftParent->right = leftChild->left;
                }
                toKill = leftChild;
            }
            else if (toKill->right != nullptr)
            {
                Node *rightChild = toKill->right;
                Node *rightParent = toKill;
                while (rightChild->left != nullptr)
                {
                    path.push_back(rightChild);
                    rightParent = rightChild;
                    rightChild = rightChild->left;
                }
                toKill->val = rightChild->val;
                if (rightParent->left == rightChild)
                {
                    rightParent->left = rightChild->right;
                }
                else
                {
                    rightParent->right = rightChild->right;
                }
                toKill = rightChild;
            }
            else
            {
                if (path.size() > 1)
                {
                    Node *parent = path[path.size() - 2];
                    if (parent->left == toKill)
                    {
                        parent->left = nullptr;
                    }
                    else
                    {
                        parent->right = nullptr;
                    }
                }
                else
                {
                    root = nullptr;
                }
            }
            success = true;

            //balance the tree
            balance(path);
            delete toKill;
        }
        return success;
    }

    void print()
    {
        if (root != nullptr)
        {
            vector<Node *> printQueue;
            vector<Node *> printQueue2;
            printQueue.push_back(root);
            int spacing = root->subtreeHeight;
            bool firstQueue = true;
            bool quit = false;
            int printCount = 0;
            while (!quit)
            {
                for (int i = 0; i < (spacing * 2) + 1; i++)
                {
                    printf("      ");
                }
                if (!firstQueue)
                {
                    Node *current = printQueue2[0];
                    printQueue2.erase(printQueue2.begin());
                    if (current == nullptr)
                    {
                        printf("      ");
                        printQueue.push_back(nullptr);
                        printQueue.push_back(nullptr);
                    }
                    else
                    {
                        printCount++;
                        printf("(%04i)", current->val);
                        printQueue.push_back(current->left);
                        printQueue.push_back(current->right);
                    }
                    if (printQueue2.empty())
                    {
                        printf("\n");
                        spacing--;
                        firstQueue = true;
                        if (printCount == 0)
                        {
                            quit = true;
                        }
                        printCount = 0;
                    }
                }
                else
                {
                    Node *current = printQueue[0];
                    printQueue.erase(printQueue.begin());
                    if (current == nullptr)
                    {
                        printf("      ");
                        printQueue2.push_back(nullptr);
                        printQueue2.push_back(nullptr);
                    }
                    else
                    {
                        printCount++;
                        printf("(%04i)", current->val);
                        printQueue2.push_back(current->left);
                        printQueue2.push_back(current->right);
                    }
                    if (printQueue.empty())
                    {
                        printf("\n");
                        spacing--;
                        firstQueue = false;
                        if (printCount == 0)
                        {
                            quit = true;
                        }
                        printCount = 0;
                    }
                }
            }
        }
        else
        {
            printf("ERROR: Nothing to Print\n");
        }
    }
};

int main(int argc, char *argv[])
{
    string command = "";
    int commandData = 0;
    AVL tree;
    printf("Please enter one of the following commands: \n\tinsert\tdelete\tfind\n\tprint\tquit\n");

    while (true)
    {
        cin >> command;

        if (command.compare("insert") == 0)
        {
            cin >> commandData;
            printf("\n");
            bool success = tree.insert(commandData);
            if (success)
            {
                printf("Node Inserted");
            }
            else
            {
                printf("ERROR: Node Not Inserted");
            }
            printf("\n");
        }
        else if (command.compare("find") == 0)
        {
            cin >> commandData;
            printf("\n");
            vector<Node *> path = tree.find(commandData);
            if (path.empty() || (path[path.size() - 1] != nullptr && path[path.size() - 1]->val != commandData))
            {
                printf("ERROR: Node Not Found");
            }
            else
            {
                printf("Node found!");
            }
            printf("\n");
        }
        else if (command.compare("delete") == 0)
        {
            cin >> commandData;
            bool success = tree.erase(commandData);
            if (success)
            {
                printf("Node Deleted");
            }
            else
            {
                printf("ERROR: Node Not Found");
            }
            printf("\n");
        }
        else if (command.compare("print") == 0)
        {
            printf("\n");
            tree.print();
            printf("\n");
        }
        else if (command.compare("quit") == 0)
        {
            printf("Goodbye!\n");
            break;
        }
        else
        {
            printf("Command not recognized. Please try again.\n");
        }

        cin.clear();
        cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        printf("\nDone!\n");
    }
    return 0;
}