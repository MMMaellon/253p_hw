#include <cstdio>    // fscanf
#include <cstdlib>   // for exit()
#include <cctype>    /* for isdigit() */
#include "myStuff.h" /* custom functions */

int main(int argc, char *argv[])
{
    //createLargeTest("numbersLarge.dat");
    double output[2] = {0.0};

    if (argc > 1)
    {
        getAverage(argv[1], output);
        fprintf(stdout, "The average of the %d numbers in file \"%s\" is %lf\n",
                (int)output[0], argv[1], (double)output[1]);
    }
    else
    {
        missingFile();
    }

    return 0;
}