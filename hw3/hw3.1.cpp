#include <iostream>
#include <string.h>
#include <sstream>
#include <fstream>
#include <stdio.h>
#include <vector>
#include <limits>
#include <unordered_set>
#include <algorithm>

using namespace std;

struct Cell
{
    bool topWall = false;
    bool botWall = false;
    bool leftWall = false;
    bool rightWall = false;
    Cell() : topWall(false), botWall(false), leftWall(false), rightWall(false) {}
};

struct Maze
{
    int width = 0;
    int height = 0;
    vector<Cell> cells;
    vector<int> solution;
    Maze() : width(0), height(0) {}
};

bool isMazeWalls(string str)
{
    bool result = false;
    bool quit = false;
    for (int i = 0; i < str.size() && !quit; i++)
    {
        switch (str.at(i))
        {
        case '_':
        case '|':
            result = true;
            break;
        case ' ':
        case '\t':
        case '\n':
        case '\r':
            break;
        default:
            result = false;
            quit = true;
            break;
        }
    }
    return result;
}

void skipInvalidLines(ifstream &file)
{
    bool validCharacter = false;
    string line;
    streampos prevLine;
    while (!file.eof())
    {
        prevLine = file.tellg();
        getline(file, line);
        if (isMazeWalls(line))
        {
            file.seekg(prevLine);
            break;
        }
    }
}

int findLeftMargin(string line)
{
    for (int i = 0; i < line.size(); i++)
    {
        if (line.at(i) != ' ')
        {
            return i;
        }
    }
    return 0;
}

int findRightMargin(string line, int leftMargin = 0)
{
    int rightMargin = 0;
    for (int i = leftMargin; i < line.size(); i++)
    {
        if (line.at(i) == '|') //find the right wall
        {
            rightMargin = line.size() - i - 1;
        }
    }
    return rightMargin;
}

Maze readFile(string filename)
{
    Maze m;
    std::printf("Opening file %s...\n", filename.c_str());
    ifstream file(filename);
    if (file)
    {
        if (file.eof())
        {
            std::printf("Warning: File is empty\n");
        }
        skipInvalidLines(file);
        //The definition of the start is so imprecise I have to assume that the top left is always the start and the bottom right is always the end
        //skipInvalidCharacters should take us directly to the line that defines the top edge of the maze and the start
        //ignore the entire line and then start reading in the maze

        int leftMargin = 0;  //how many whitespaces are before the left edge of the maze
        int rightMargin = 0; //how many whitespaces are after the right edge of the maze

        string line;
        string prevLine;
        while (!file.eof())
        {
            getline(file, line);
            if (m.width == 0)
            {
                leftMargin = findLeftMargin(line);
                rightMargin = findRightMargin(line, leftMargin);
                //set the width
                m.width = (line.size() - leftMargin - rightMargin - 1) / 2; //this calculation is based off the example that used (2n + 1) characters to represent n cells
            }
            bool isEnding = false;
            for (int cellIndex = 0; cellIndex < m.width; cellIndex++)
            {
                Cell cell;
                int charIndex = leftMargin + (cellIndex * 2) + 1;
                if (prevLine.empty() || prevLine[charIndex] == '_') //at the top row prevLine would be empty
                {
                    cell.topWall = true;
                }
                if (line[charIndex] == '_')
                {
                    cell.botWall = true;
                }
                else if (line[charIndex] == 'e')
                {
                    //we hit the ending line
                    isEnding = true;
                    break;
                }
                if (cellIndex == 0 || line[charIndex - 1] == '|')
                {
                    cell.leftWall = true;
                }
                if (cellIndex == m.width || line[charIndex + 1] == '|')
                {
                    cell.rightWall = true;
                }
                m.cells.push_back(cell);
            }
            if (isEnding)
            {
                break;
            }
            else
            {
                prevLine = line;
                m.height++;
            }
        }
    }
    else
    {
        std::printf("Could not read file %s\n", filename.c_str());
    }
    file.close();
    return m;
}

Maze readFileFromStdin()
{
    Maze m;
    std::printf("Please type the filename of the maze data: ");
    string filename;
    while (filename.empty())
    {
        getline(cin, filename);
    }
    m = readFile(filename);
    if (m.cells.size() == 0)
    {
        std::printf("ERROR: The Maze File was empty or could not be read\n");
    }
    return m;
}

bool isInSolution(vector<int> solution, int value)
{
    for (int i = 0; i < solution.size(); i++)
    {
        if (solution[i] == value)
        {
            return true;
        }
    }
    return false;
}

vector<int> recursiveDepthFirstSearch(Maze &m, vector<int> partialSolution, int nextCell)
{
    if (!isInSolution(partialSolution, nextCell))
    {
        partialSolution.push_back(nextCell);
    }
    else
    {
        return vector<int>();
    }
    if (nextCell != m.width * m.height - 1) //if we are not at the goal state
    {
        vector<int> pathup;
        vector<int> pathdown;
        vector<int> pathleft;
        vector<int> pathright;

        if (nextCell >= m.width && !m.cells[nextCell].topWall)
        {
            pathup = recursiveDepthFirstSearch(m, partialSolution, nextCell - m.width);
        }
        if (nextCell < (m.width) * (m.height - 1) && !m.cells[nextCell].botWall)
        {
            pathdown = recursiveDepthFirstSearch(m, partialSolution, nextCell + m.width);
        }
        if (nextCell % m.width != 0 && !m.cells[nextCell].leftWall)
        {
            pathleft = recursiveDepthFirstSearch(m, partialSolution, nextCell - 1);
        }
        if (nextCell + 1 % m.width != 0 && !m.cells[nextCell].rightWall)
        {
            pathright = recursiveDepthFirstSearch(m, partialSolution, nextCell + 1);
        }
        if (!pathup.empty())
        {
            partialSolution = pathup;
        }
        else if (!pathdown.empty())
        {
            partialSolution = pathdown;
        }
        else if (!pathleft.empty())
        {
            partialSolution = pathleft;
        }
        else if (!pathright.empty())
        {
            partialSolution = pathright;
        }
        else
        {
            partialSolution.clear();
        }
    }
    return partialSolution;
}

void solveMaze(Maze &m)
{
    m.solution = recursiveDepthFirstSearch(m, vector<int>(), 0); //starting cell is always 0
}

void printMap(Maze &m)
{
    char *indentString = (char *)"    |"; //always indent 4
    std::printf("(start)");
    for (int col = 1; col < m.width; col++)
    {
        std::printf("_ ");
    }
    std::printf("\n");
    for (int row = 0; row < m.height; row++)
    {
        std::printf(indentString);
        for (int col = 0; col < m.width; col++)
        {
            int index = (row * m.width) + col;
            if (m.cells[index].botWall)
            {
                std::printf("_");
            }
            else
            {
                std::printf(" ");
            }
            if (m.cells[index].rightWall)
            {
                std::printf("|");
            }
            else
            {
                std::printf(" ");
            }
        }
        std::printf("\n");
    }
    std::printf("%s(end)\n", string(3 + m.width * 2, ' ').c_str());
}

void printSolutionVector(vector<int> solution)
{
    printf("Solution: ");
    for (int i = 0; i < solution.size(); i++)
    {
        if (i == 0)
        {
            printf("%d", solution[i]);
        }
        else
        {
            printf(", %d", solution[i]);
        }
    }
    printf("\n");
}

void printSolutionMap(Maze &m)
{
    char *indentString = (char *)"    |"; //always indent 4
    unordered_set<int> solutionSet(m.solution.begin(), m.solution.end());
    std::printf("(start)");
    for (int col = 1; col < m.width; col++)
    {
        std::printf("_ ");
    }
    std::printf("\n");
    for (int row = 0; row < m.height; row++)
    {
        std::printf(indentString);
        for (int col = 0; col < m.width; col++)
        {
            int index = (row * m.width) + col;
            if (solutionSet.count(index) == 1)
            {
                //Part of the Solution
                std::printf("X ");
            }
            else
            {
                //Not Part of the Solution
                std::printf("  ");
            }
        }
        std::printf("\n");
    }
    std::printf("%s(end)\n", string(3 + m.width * 2, ' ').c_str());
}

void printMaze(Maze &m)
{
    printMap(m);
    if (m.solution.size() > 0)
    {
        printSolutionVector(m.solution);
        printSolutionMap(m);
    }
    else
    {
        printf("No solution could be found :(\n");
    }
}

int main(int argc, char *argv[])
{
    Maze m;
    if (argc >= 2)
    {
        m = readFile(argv[1]);
    }
    while (m.cells.size() == 0)
    {
        m = readFileFromStdin();
    }
    solveMaze(m);
    printMaze(m);
}