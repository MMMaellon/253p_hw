#include <iostream>
#include <string.h>
#include <sstream>
#include <fstream>
#include <stdio.h>
#include <vector>

using namespace std;

bool is_multiOperator(char c)
{
    switch (c)
    {
    case '!':
    case '|':
    case '&':
    case '*':
    case '/':
    case '+':
    case '-':
    case '<':
    case '>':
    case '=':
        return true;
    default:
        return false;
    }
}

bool is_operator(char c)
{
    switch (c)
    {
    case '#':
    case '!':
    case '|':
    case '&':
    case '*':
    case '/':
    case '+':
    case '-':
    case '<':
    case '>':
    case '=':
    case '[':
    case '(':
    case '{':
    case ']':
    case ')':
    case '}':
    case ';':
    case ':':
    case ',':
    case '.':
    case '?':
        return true;
    default:
        return false;
    }
}

bool is_num(char c)
{
    switch (c)
    {
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
    case '.':
        return true;
    default:
        return false;
    }
}

void addToken(vector<string> &tokens, string &token)
{
    if (token.size() > 0)
    {
        tokens.push_back(token);
        token.clear();
    }
}

vector<string> parseC(ifstream &file)
{
    vector<string> tokens;
    bool multilineComment = false;
    while (!file.eof())
    {
        string line;
        getline(file, line);
        string token;
        bool op = false; //op is short for operator
        bool num = false;
        bool singleQuot = false;
        bool doubleQuot = false;
        bool escapedQuot = false;
        bool alphaNum = false;
        bool comment = false;
        for (int i = 0; i < line.size() && !comment; i++)
        {
            if (singleQuot || doubleQuot)
            {
                if (escapedQuot)
                {
                    token += line[i];
                    escapedQuot = false;
                }
                else
                {
                    token += line[i];
                    if ((singleQuot && line[i] == '\'') || (doubleQuot && line[i] == '"'))
                    {
                        addToken(tokens, token);
                        singleQuot = false;
                        doubleQuot = false;
                    }
                    else if (line[i] == '\\')
                    {
                        escapedQuot = true;
                    }
                }
            }
            else
            {
                switch (line[i])
                {
                case ' ':
                case '\t':
                case '\n':
                case '\r':
                    //whitespace terminates whatever was before it
                    addToken(tokens, token);
                    break;
                default:
                    if (multilineComment)
                    {
                        if (line[i] == '*' && i + 1 < line.size() && (line[i + 1] == '/'))
                        {
                            multilineComment = false;
                            i++;
                        }
                    }
                    else if (line[i] == '\'')
                    {
                        addToken(tokens, token);
                        token = "'";
                        singleQuot = true;
                    }
                    else if (line[i] == '"')
                    {
                        addToken(tokens, token);
                        token = "\"";
                        doubleQuot = true;
                    }
                    else if (is_operator(line[i]))
                    {
                        if (alphaNum || num)
                        {
                            if (num && line[i] == '.')
                            {
                                token += line[i];
                                break;
                            }
                            else
                            {
                                addToken(tokens, token);
                                alphaNum = false;
                                num = false;
                            }
                        }
                        op = true;
                        if (is_multiOperator(line[i]) && i < line.size() - 1 && is_multiOperator(line[i + 1]))
                        {
                            if (line[i] == '/' && (line[i + 1] == '/'))
                            {
                                comment = true;

                                op = false;
                                num = false;
                                singleQuot = false;
                                doubleQuot = false;
                                escapedQuot = false;
                                alphaNum = false;
                            }
                            else if (line[i] == '/' && (line[i + 1] == '*'))
                            {
                                multilineComment = true;

                                op = false;
                                num = false;
                                singleQuot = false;
                                doubleQuot = false;
                                escapedQuot = false;
                                alphaNum = false;
                            }
                            else
                            {
                                token += line[i];
                                token += line[i + 1];
                                addToken(tokens, token);
                                i++; //skip next character because it was part of the multichar operator
                            }
                        }
                        else
                        {
                            addToken(tokens, token);
                            token += line[i];
                            addToken(tokens, token);
                        }
                    }
                    else if (is_num(line[i]))
                    {
                        if (!num && !alphaNum)
                        {
                            addToken(tokens, token);
                            num = true;
                        }
                        token += line[i];
                    }
                    else
                    {
                        if (op || num)
                        {
                            addToken(tokens, token);
                        }
                        op = false;
                        num = false;
                        alphaNum = true;
                        token += line[i];
                    }
                }
            }
        }
        addToken(tokens, token);
    }
    return tokens;
}

vector<string> openFile(int argc, char *argv[])
{
    string filename;
    if (argc > 1)
    {
        filename = argv[1];
    }
    else
    {
        std::printf("Please type the filename of the C++ file: ");
    }
    ifstream file(filename);
    while (!file)
    {
        getline(cin, filename);
        file = ifstream(filename);
        if (!file)
        {
            std::printf("ERROR: The file could not be read\n");
        }
    }
    return parseC(file);
}
void printTokens(vector<string> tokens)
{
    for (int i = 0; i < tokens.size(); i++)
    {
        printf("%s\n", tokens[i].c_str());
    }
}

int main(int argc, char *argv[])
{
    vector<string> tokens = openFile(argc, argv);
    printTokens(tokens);
}