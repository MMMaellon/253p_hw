to build:
g++ hw3.1.cpp -o hw3.1
g++ hw3.2.cpp -o hw3.2

to run:
hw3.1
hw3.2

to pass in a file use stdin or write the following
hw3.1 <input file>
hw3.2 <input file>