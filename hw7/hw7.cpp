#include <string>
#include <iostream>
#include <sstream>

using namespace std;

int romanToInt(string romans)
{
    int result = 0;
    for (int i = 0; i < romans.size(); i++)
    {
        switch (romans[i])
        {
        case 'V':
            if (i != 0 && romans[i - 1] == 'I')
            {
                result += 3;
            }
            else
            {
                result += 5;
            }
            break;
        case 'X':
            if (i != 0 && romans[i - 1] == 'I')
            {
                result += 8;
            }
            else
            {
                result += 10;
            }
            break;
        case 'L':
            if (i != 0 && romans[i - 1] == 'X')
            {
                result += 30;
            }
            else
            {
                result += 50;
            }
            break;
        case 'C':
            if (i != 0 && romans[i - 1] == 'X')
            {
                result += 80;
            }
            else
            {
                result += 100;
            }
            break;
        case 'D':
            if (i != 0 && romans[i - 1] == 'C')
            {
                result += 300;
            }
            else
            {
                result += 500;
            }
            break;
        case 'M':
            if (i != 0 && romans[i - 1] == 'C')
            {
                result += 800;
            }
            else
            {
                result += 1000;
            }
            break;
        default:
            result++;
        }
    }
    return result;
}

string intToRoman(int num)
{
    string romans;
    while (num > 0)
    {
        if (num >= 1000)
        {
            romans.insert(romans.size(), num / 1000, 'M');
            num = num % 1000;
        }
        else if (num >= 900)
        {
            romans += "CM";
            num -= 900;
        }
        else if (num >= 500)
        {
            romans.insert(romans.size(), num / 500, 'D');
            num = num % 500;
        }
        else if (num >= 400)
        {
            romans += "CD";
            num -= 400;
        }
        else if (num >= 100)
        {
            romans.insert(romans.size(), num / 100, 'C');
            num = num % 100;
        }
        else if (num >= 90)
        {
            romans += "XC";
            num -= 90;
        }
        else if (num >= 50)
        {
            romans.insert(romans.size(), num / 50, 'L');
            num = num % 50;
        }
        else if (num >= 40)
        {
            romans += "XL";
            num -= 40;
        }
        else if (num >= 10)
        {
            romans.insert(romans.size(), num / 10, 'X');
            num = num % 10;
        }
        else if (num >= 9)
        {
            romans += "IX";
            num -= 9;
        }
        else if (num >= 5)
        {
            romans.insert(romans.size(), num / 5, 'V');
            num = num % 5;
        }
        else if (num >= 4)
        {
            romans += "IV";
            num -= 4;
        }
        else
        {
            romans.insert(romans.size(), num, 'I');
            num = 0;
        }
    }
    return romans;
}

bool validNumber(string s)
{
    for (char c : s)
    {
        switch (c)
        {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            break;
        default:
            return false;
        }
    }
    return true;
}

bool validNumeral(string s)
{
    for (char c : s)
    {
        switch (c)
        {
        case 'M':
        case 'C':
        case 'D':
        case 'X':
        case 'L':
        case 'I':
        case 'V':
            break;
        default:
            return false;
        }
    }
    return true;
}

int main(int argc, char *argv[])
{
    string line;
    while (line.compare("quit") != 0)
    {
        printf("Enter a Roman Numeral or an integer. Negative numbers are not allowed: \n");
        getline(cin, line);
        if (validNumber(line))
        {
            int num;
            stringstream ss(line);
            ss >> num;
            printf("Roman Numeral: %s\n", intToRoman(num).c_str());
        }
        else if (validNumeral(line))
        {
            printf("Integer: %i\n", romanToInt(line));
        }
        else if (line.compare("quit") == 0)
        {
            break;
        }
        else
        {
            printf("Unable to parse input!\n");
        }
    }
    printf("Goodbye!\n");
    return 0;
}