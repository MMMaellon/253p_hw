#include <iostream>
#include <string.h>
#include <stdio.h>
#include <cstdlib>
#include <list>
#include <unordered_map>

using namespace std;

struct Song
{
    string title;
    string artist;
    Song(string p_title = string(), string p_artist = string()) : title(p_title), artist(p_artist) {}

    string print()
    {
        return title + " - " + artist;
    }

    bool equals(Song const &other)
    {
        return other.title.compare(title) + other.artist.compare(artist) == 0;
    }

    static bool parse(string rawInput, Song &song, Song *song2 = nullptr)
    {
        string title, artist;
        bool parsingArtist = false;
        bool trailingWhitespace = false;
        bool secondParseFail = false;
        for (int i = 0; i < rawInput.size(); i++)
        {
            if (title.empty())
            {
                switch (rawInput[i])
                {
                case ' ':
                case '\t':
                case '\n':
                case '\r':
                    //skip whitespace
                    break;
                case '[': //bad formatting
                    return false;
                default:
                    title += rawInput[i];
                }
            }
            else if (!parsingArtist && artist.empty())
            {
                switch (rawInput[i])
                {
                case '[':
                    parsingArtist = true;
                    break;
                case ']':
                    //bad formatting
                    return false;
                case ' ':
                case '\t':
                case '\n':
                case '\r':
                    trailingWhitespace = true;
                    break;
                default:
                    if (trailingWhitespace)
                    {
                        title += ' ';
                    }
                    trailingWhitespace = false;
                    title += rawInput[i];
                }
            }
            else if (parsingArtist)
            {
                switch (rawInput[i])
                {
                case ']':
                    parsingArtist = false;
                    break;
                default:
                    artist += rawInput[i];
                }
            }
            else
            {
                if (song2 != nullptr)
                {
                    secondParseFail = !parse(rawInput.substr(i), *song2);
                }
                break;
            }
        }
        if (artist.empty() || parsingArtist || secondParseFail)
        {
            return false;
        }
        song.title = title;
        song.artist = artist;
        return true;
    }
};

class AdvancedPlayList
{
private:
    list<Song> songs;
    unordered_map<string, list<Song>::iterator> indexes;
    list<Song>::iterator current;
    int size;

public:
    AdvancedPlayList() : songs(), indexes(), current(), size(0) {}

    void printPlacement(list<Song>::iterator curr, const char *prompt = "Current Song")
    {
        if (size > 0)
        {

            list<Song>::iterator prev;
            list<Song>::iterator next = std::next(curr);
            if (curr == songs.begin())
            {
                prev = std::prev(songs.end());
            }
            else
            {
                prev = std::prev(curr);
            }
            if (next == songs.end())
            {
                next = songs.begin();
            }
            printf("Previous Song: %s\n", prev->print().c_str());
            printf("%s: %s\n", prompt, curr->print().c_str());
            printf("Next Song: %s\n", next->print().c_str());
        }
    }

    void pushSong(Song &song)
    {
        if (indexes.count(song.print()) == 0)
        {
            songs.push_front(song);
            indexes[song.print()] = songs.begin();
            if (size == 0)
            {
                current = songs.begin();
            }
            size++;
        }
        else
        {
            printf("Error: %s is already in the playlist\n", song.print().c_str());
        }
    }

    void queueSong(Song &song)
    {
        if (indexes.count(song.print()) == 0)
        {
            songs.push_back(song);
            indexes[song.print()] = std::prev(songs.end());
            if (size == 0)
            {
                current = songs.begin();
            }
            size++;
        }
        else
        {
            printf("Error: %s is already in the playlist\n", song.print().c_str());
        }
    }

    void currentSong()
    {
        if (size > 0)
        {
            printPlacement(current);
        }
        else
        {
            printf("No Current Song\n");
        }
    }

    void deleteSong()
    {
        if (size > 0 && current != songs.end())
        {
            auto oldCurrent = current;
            current++;
            if (current == songs.end())
            {
                if (size > 1)
                {
                    current = songs.begin();
                }
            }
            indexes.erase(oldCurrent->print());
            songs.erase(oldCurrent);
            size--;
        }
        else
        {
            printf("Error: No Current Song\n");
        }
    }

    void prevSong()
    {
        if (size > 0)
        {
            if (current != songs.begin())
            {
                current--;
            }
            else
            {
                current = std::prev(songs.end());
            }
        }
        else
        {
            printf("Error: No Current Song\n");
        }
    }

    void nextSong()
    {
        if (size > 0)
        {
            current++;
            if (current == songs.end())
            {
                current = songs.begin();
            }
        }
        else
        {
            printf("Error: No Current Song\n");
        }
    }

    void restart()
    {
        current = songs.begin();
    }

    void findSong(Song &song)
    {
        if (size > 0)
        {
            if (indexes.count(song.print()) > 0)
            {
                list<Song>::iterator s = indexes[song.print()];
                printPlacement(s, "Song");
            }
            else
            {
                printf("Error: Could Not Find Song %s\n", song.print().c_str());
            }
        }
        else
        {
            printf("Error: Empty Song Bank\n");
        }
    }

    void changeTo(Song &song)
    {
        if (size > 0)
        {
            if (indexes.count(song.print()) > 0)
            {
                list<Song>::iterator s = indexes[song.print()];
                current = s;
            }
            else
            {
                printf("Error: Could Not Find Song %s\n", song.print().c_str());
            }
        }
        else
        {
            printf("Error: Empty Song Bank\n");
        }
    }

    void addBefore(Song &song, Song &searchSong)
    {
        if (size > 0)
        {
            if (indexes.count(song.print()) == 0)
            {
                if (indexes.count(searchSong.print()) > 0)
                {
                    list<Song>::iterator place = indexes[searchSong.print()];
                    songs.insert(place, song);
                    indexes[song.print()] = std::prev(place);
                    size++;
                }
                else
                {
                    printf("Error: Could Not Find Song %s\n", searchSong.print().c_str());
                }
            }
            else
            {
                printf("Error: %s is already in the playlist\n", song.print().c_str());
            }
        }
        else
        {
            printf("Error: Empty Song Bank\n");
        }
    }

    void addAfter(Song &song, Song &searchSong)
    {
        if (size > 0)
        {
            if (indexes.count(song.print()) == 0)
            {
                if (indexes.count(searchSong.print()) > 0)
                {
                    list<Song>::iterator place = indexes[searchSong.print()];
                    songs.insert(std::next(place), song);
                    indexes[song.print()] = std::next(place);
                    size++;
                }
                else
                {
                    printf("Error: Could Not Find Song %s\n", searchSong.print().c_str());
                }
            }
            else
            {
                printf("Error: %s is already in the playlist\n", song.print().c_str());
            }
        }
        else
        {
            printf("Error: Empty Song Bank\n");
        }
    }

    void random()
    {
        if (size > 1)
        {
            int jumps = (rand() % (size - 1)) + 1;
            for (int i = 0; i < jumps; i++)
            {
                nextSong();
            }
        }
    }

    void print()
    {
        if (size > 0)
        {
            list<Song>::iterator iter = songs.begin();
            int i = 1;
            while (iter != songs.end())
            {
                printf("%i) %s\t\n", i++, iter->print().c_str());
                iter++;
            }
        }
        else
        {
            printf("The playlist is empty\n");
        }
    }
};

Song getSongFromInput(string commandData, Song *song2 = nullptr)
{
    Song s;
    while (!Song::parse(commandData, s, song2))
    {
        if (song2 == nullptr)
        {
            printf("Please enter the Song Title and Artist like this: \"Song Title [Artist Name]\": ");
        }
        else
        {
            printf("Please enter the Song Title and Artist like this: \"Search Song [Artist Name] Song To Add [Artist Name 2]\": ");
        }
        getline(cin, commandData);
    }
    return s;
}

int main(int argc, char *argv[])
{
    string command;
    string commandData;
    AdvancedPlayList playlist;
    printf("Please enter one of the following commands: \n\tpush\tqueue\n\tfind\tprint\tcurrent\tchangeTo\n\tprev\tnext\trandom\trestart\n\taddBefore\taddAfter\n\tdelete\tquit\n");

    while (true)
    {
        //printf("-------------------------------------------\n");
        //printf("Please enter one of the following commands: \n\tpush\tqueue\n\tfind\tprint\tcurrent\tchangeTo\n\tprev\tnext\trandom\trestart\n\taddBefore\taddAfter\n\tdelete\tquit\n");
        cin >> command;
        getline(cin, commandData);
        //printf("\n\n\n\n");

        if (command.compare("push") == 0)
        {
            Song s = getSongFromInput(commandData);
            playlist.pushSong(s);
        }
        else if (command.compare("queue") == 0)
        {
            Song s = getSongFromInput(commandData);
            playlist.queueSong(s);
        }
        else if (command.compare("current") == 0)
        {
            playlist.currentSong();
        }
        else if (command.compare("delete") == 0)
        {
            playlist.deleteSong();
        }
        else if (command.compare("prev") == 0)
        {
            playlist.prevSong();
        }
        else if (command.compare("next") == 0)
        {
            playlist.nextSong();
        }
        else if (command.compare("restart") == 0)
        {
            playlist.restart();
        }
        else if (command.compare("find") == 0)
        {
            Song s = getSongFromInput(commandData);
            playlist.findSong(s);
        }
        else if (command.compare("changeTo") == 0)
        {
            Song s = getSongFromInput(commandData);
            playlist.changeTo(s);
        }
        else if (command.compare("addBefore") == 0)
        {
            Song songToAdd;
            Song searchSong = getSongFromInput(commandData, &songToAdd);
            playlist.addBefore(songToAdd, searchSong);
        }
        else if (command.compare("addAfter") == 0)
        {
            Song songToAdd;
            Song searchSong = getSongFromInput(commandData, &songToAdd);
            playlist.addAfter(songToAdd, searchSong);
        }
        else if (command.compare("random") == 0)
        {
            playlist.random();
        }
        else if (command.compare("print") == 0)
        {
            playlist.print();
        }
        else if (command.compare("quit") == 0)
        {
            printf("\n\nGoodbye!\n");
            break;
        }
        else
        {
            printf("Command not recognized. Please try again.\n");
        }
        printf("\nDone!\n");
    }
    return 0;
}