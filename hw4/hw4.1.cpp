#include <iostream>
#include <string.h>
#include <stdio.h>
#include <cstdlib>

using namespace std;

struct Song
{
    string title;
    string artist;
    Song(string p_title = string(), string p_artist = string()) : title(p_title), artist(p_artist) {}

    string print()
    {
        return title + " - " + artist;
    }

    bool equals(Song const &other)
    {
        return other.title.compare(title) + other.artist.compare(artist) == 0;
    }

    static bool parse(string rawInput, Song &song, Song *song2 = nullptr)
    {
        string title, artist;
        bool parsingArtist = false;
        bool trailingWhitespace = false;
        bool secondParseFail = false;
        for (int i = 0; i < rawInput.size(); i++)
        {
            if (title.empty())
            {
                switch (rawInput[i])
                {
                case ' ':
                case '\t':
                case '\n':
                case '\r':
                    //skip whitespace
                    break;
                case '[': //bad formatting
                    return false;
                default:
                    title += rawInput[i];
                }
            }
            else if (!parsingArtist && artist.empty())
            {
                switch (rawInput[i])
                {
                case '[':
                    parsingArtist = true;
                    break;
                case ']':
                    //bad formatting
                    return false;
                case ' ':
                case '\t':
                case '\n':
                case '\r':
                    trailingWhitespace = true;
                    break;
                default:
                    if (trailingWhitespace)
                    {
                        title += ' ';
                    }
                    trailingWhitespace = false;
                    title += rawInput[i];
                }
            }
            else if (parsingArtist)
            {
                switch (rawInput[i])
                {
                case ']':
                    parsingArtist = false;
                    break;
                default:
                    artist += rawInput[i];
                }
            }
            else
            {
                if (song2 != nullptr)
                {
                    secondParseFail = !parse(rawInput.substr(i), *song2);
                }
                break;
            }
        }
        if (artist.empty() || parsingArtist || secondParseFail)
        {
            return false;
        }
        song.title = title;
        song.artist = artist;
        return true;
    }
};

struct Node;

struct NodePlacement
{
    Node *prev = nullptr;
    Node *curr = nullptr;
};

struct Node
{
public:
    Song song;
    Node *next;

    Node(Song p_song, Node *p_next) : song(p_song), next(p_next) {}
    bool equals(Node *other)
    {
        return other ? song.equals(other->song) : false;
    }

    NodePlacement find(Song const &s)
    {
        NodePlacement place;
        place.curr = this;
        while (place.curr != nullptr && !place.curr->song.equals(s))
        {
            place.prev = place.curr;
            place.curr = place.curr->next;
        }
        if (place.curr == nullptr)
        { //it was never found
            place.prev = nullptr;
        }
        else
        {
            if (place.prev == nullptr)
            { //it was the first node that we checked which means prev needs to be the last item in the list
                place.prev = place.curr;
                while (place.prev->next != nullptr)
                {
                    place.prev = place.prev->next;
                }
            }
        }
        return place;
    }

    void insertAfter(Song const &s)
    {
        next = new Node(s, next);
    }

    string print()
    {
        return song.print();
    }
};

class SimplePlayList
{
private:
    Node *head;
    Node *current;
    int size;

public:
    SimplePlayList() : head(nullptr), current(nullptr), size(0) {}
    ~SimplePlayList()
    {
        current = nullptr;
        while (head != nullptr)
        {
            Node *next = head->next;
            delete head;
            head = next;
        }
        head = nullptr;
    }

    void printPlacement(NodePlacement &place, const char *prompt = "Current Song")
    {
        printf("Previous Song: %s\n", place.prev->print().c_str());
        printf("%s: %s\n", prompt, place.curr->print().c_str());
        if (place.curr->next != nullptr)
        {
            printf("Next Song: %s\n", place.curr->next->print().c_str());
        }
        else
        {
            //wrap around to start
            printf("Next Song: %s\n", head->print().c_str());
        }
    }

    void pushSong(Song const &song)
    {
        head = new Node(song, head);
        if (head->next == nullptr)
        {
            current = head;
        }
        size++;
    }

    void queueSong(Song const &song)
    {
        if (head == nullptr)
        {
            pushSong(song);
        }
        else
        {
            Node *tail = head;
            while (tail->next != nullptr)
            {
                tail = tail->next;
            }
            tail->insertAfter(song);
        }
        size++;
    }

    void currentSong()
    {
        NodePlacement place;
        if (head != nullptr && current != nullptr)
        {
            place = head->find(current->song);
        }
        if (place.prev != nullptr && place.curr != nullptr)
        {
            printPlacement(place);
        }
        else
        {
            printf("No Current Song\n");
        }
    }

    void deleteSong()
    {
        if (current != nullptr)
        {
            if (current->next != nullptr)
            {
                Node *nextNext = current->next->next;
                current->song = current->next->song;
                delete current->next;
                current->next = nextNext;
            }
            else
            {
                //the one edgecase where it takes O(n) time
                NodePlacement place = head->find(current->song);
                if (place.prev->next != nullptr)
                {
                    delete place.curr;
                    place.curr = nullptr;
                    place.prev->next = nullptr;
                    current = head;
                }
                else
                {
                    //we're deleting the only node in the list
                    delete current;
                    head = nullptr;
                    current = nullptr;
                }
            }
            size--;
        }
        else
        {
            printf("Error: No Current Song\n");
        }
    }

    void prevSong()
    {
        if (current != nullptr)
        {
            NodePlacement place = head->find(current->song);
            current = place.prev;
        }
        else
        {
            printf("Error: No Current Song\n");
        }
    }

    void nextSong()
    {
        if (current != nullptr)
        {
            if (current->next != nullptr)
            {
                current = current->next;
            }
            else
            {
                current = head;
            }
        }
        else
        {
            printf("Error: No Current Song\n");
        }
    }

    void restart()
    {
        current = head;
    }

    void findSong(Song &song)
    {
        if (head != nullptr)
        {
            NodePlacement place = head->find(song);
            if (place.curr != nullptr)
            {
                printPlacement(place, "Song");
            }
            else
            {
                printf("Error: Could not find song %s\n", song.print().c_str());
            }
        }
        else
        {
            printf("Error: Empty Song Bank\n");
        }
    }

    void changeTo(Song &song)
    {
        if (head != nullptr)
        {
            NodePlacement place = head->find(song);
            if (place.curr != nullptr)
            {
                current = place.curr;
            }
            else
            {
                printf("Error: Could not find song %s\n", song.print().c_str());
            }
        }
        else
        {
            printf("Error: Empty Song Bank\n");
        }
    }

    void addBefore(Song &song, Song &searchSong)
    {
        if (head != nullptr)
        {
            NodePlacement place = head->find(searchSong);
            if (place.curr != nullptr)
            {
                if (place.prev->next != nullptr)
                {
                    place.prev->insertAfter(song);
                    size++;
                }
                else
                {
                    //we wrapped around
                    pushSong(song);
                }
            }
            else
            {
                printf("Error: Could not find song %s\n", song.print().c_str());
            }
        }
        else
        {
            printf("Error: Empty Song Bank\n");
        }
    }

    void addAfter(Song &song, Song &searchSong)
    {
        if (head != nullptr)
        {
            NodePlacement place = head->find(searchSong);
            if (place.curr != nullptr)
            {
                place.curr->insertAfter(song);
                size++;
            }
            else
            {
                printf("Error: Could not find song %s\n", song.print().c_str());
            }
        }
        else
        {
            printf("Error: Empty Song Bank\n");
        }
    }

    void random()
    {
        if (size > 1)
        {
            int jumps = rand() % size;
            for (int i = 0; i < jumps; i++)
            {
                nextSong();
            }
        }
    }

    void print()
    {
        if (head != nullptr)
        {
            Node *n = head;
            int i = 1;
            while (n != nullptr)
            {
                printf("%i) %s\t\n", i++, n->print().c_str());
                n = n->next;
            }
        }
        else
        {
            printf("The playlist is empty\n");
        }
    }
};

Song getSongFromInput(string commandData, Song *song2 = nullptr)
{
    Song s;
    while (!Song::parse(commandData, s, song2))
    {
        if (song2 == nullptr)
        {
            printf("Please enter the Song Title and Artist like this: \"Song Title [Artist Name]\": ");
        }
        else
        {
            printf("Please enter the Song Title and Artist like this: \"Search Song [Artist Name] Song To Add [Artist Name 2]\": ");
        }
        getline(cin, commandData);
    }
    return s;
}

int main(int argc, char *argv[])
{
    string command;
    string commandData;
    SimplePlayList playlist;
    printf("Please enter one of the following commands: \n\tpush\tqueue\n\tfind\tprint\tcurrent\tchangeTo\n\tprev\tnext\trandom\trestart\n\taddBefore\taddAfter\n\tdelete\tquit\n");

    while (true)
    {
        //printf("-------------------------------------------\n");
        //printf("Please enter one of the following commands: \n\tpush\tqueue\n\tfind\tprint\tcurrent\tchangeTo\n\tprev\tnext\trandom\trestart\n\taddBefore\taddAfter\n\tdelete\tquit\n");
        cin >> command;
        getline(cin, commandData);
        //printf("\n\n\n\n");

        if (command.compare("push") == 0)
        {
            playlist.pushSong(getSongFromInput(commandData));
        }
        else if (command.compare("queue") == 0)
        {
            playlist.queueSong(getSongFromInput(commandData));
        }
        else if (command.compare("current") == 0)
        {
            playlist.currentSong();
        }
        else if (command.compare("delete") == 0)
        {
            playlist.deleteSong();
        }
        else if (command.compare("prev") == 0)
        {
            playlist.prevSong();
        }
        else if (command.compare("next") == 0)
        {
            playlist.nextSong();
        }
        else if (command.compare("restart") == 0)
        {
            playlist.restart();
        }
        else if (command.compare("find") == 0)
        {
            Song s = getSongFromInput(commandData);
            playlist.findSong(s);
        }
        else if (command.compare("changeTo") == 0)
        {
            Song s = getSongFromInput(commandData);
            playlist.changeTo(s);
        }
        else if (command.compare("addBefore") == 0)
        {
            Song songToAdd;
            Song searchSong = getSongFromInput(commandData, &songToAdd);
            playlist.addBefore(songToAdd, searchSong);
        }
        else if (command.compare("addAfter") == 0)
        {
            Song songToAdd;
            Song searchSong = getSongFromInput(commandData, &songToAdd);
            playlist.addAfter(songToAdd, searchSong);
        }
        else if (command.compare("random") == 0)
        {
            playlist.random();
        }
        else if (command.compare("print") == 0)
        {
            playlist.print();
        }
        else if (command.compare("quit") == 0)
        {
            printf("\n\nGoodbye!\n");
            break;
        }
        else
        {
            printf("Command not recognized. Please try again.\n");
        }
        printf("\nDone!\n");
    }
    return 0;
}