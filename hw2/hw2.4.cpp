#include <string>
#include <iostream>
#include <vector>

using namespace std;

bool matchingSubstrings(string str, int offset, int subStringSize)
{
    bool matching = false;
    if (subStringSize <= offset && offset < str.size())
    {
        matching = true;
        for (int i = 0; i < subStringSize; i++)
        {
            char a = str.at(i);
            char b = str.at(offset - subStringSize + i + 1);
            if (a != b)
            {
                matching = false;
                break;
            }
        }
    }
    return matching;
}

vector<int> calcJumpVector(string str)
{
    vector<int> jumpVector;
    for (int offset = 0; offset < str.size(); offset++)
    {
        jumpVector.push_back(0);
        for (int subStringSize = 1; subStringSize <= offset; subStringSize++)
        {
            if (matchingSubstrings(str, offset, subStringSize))
            {
                jumpVector[offset] = subStringSize;
            }
        }
    }
    return jumpVector;
}

void printVector(vector<int> v)
{
    for (int i = 0; i < v.size(); i++)
    {
        if (i == 0)
        {
            printf("%i", v[i]);
        }
        else
        {
            printf(", %i", v[i]);
        }
    }

    if (v.size() == 0)
    {
        printf("Nothing to print");
    }
    printf("\n");
}

int_least16_t findNextKMPMatch(string needle, char charToMatch, vector<int> jumpVector, int currentIndex){
    int newIndex = currentIndex;
    while (newIndex > 0)
    {
        newIndex = jumpVector[newIndex - 1];
        char needleChar = needle.at(newIndex);
        if (needleChar == charToMatch)
        {
            return newIndex + 1;
        }
    }
    return 0;
}

vector<int> kmp(string needle, string haystack)
{
    vector<int> indexes;
    if (needle.size() <= haystack.size())
    {
        vector<int> jumpVector = calcJumpVector(needle);
        int needleIndex = 0;
        char needleChar, hayChar;
        for (int i = 0; i < haystack.size(); i++)
        {
            if (needle.at(needleIndex) == haystack.at(i))
            {
                if (needleIndex >= needle.size() - 1)
                {
                    //we made it to the end of the needle which means we have a match
                    indexes.push_back(i - needleIndex);
                    needleIndex = jumpVector[needleIndex];
                }
                else
                {
                    needleIndex++;
                }
            }
            else
            {
                //If we don't have a match, but if some known prefix matches we wanna jump to it using the jumpvector
                needleIndex = findNextKMPMatch(needle, haystack.at(i), jumpVector, needleIndex);
            }
        }
    }
    return indexes;
}

int main(int argc, char *argv[])
{
    string needle, haystack;
    printf("please enter the needle: \n");
    getline(cin, needle);
    printf("please enter the haystack: \n");
    getline(cin, haystack);
    vector<int> indexes = kmp(needle, haystack);
    printVector(indexes);
}