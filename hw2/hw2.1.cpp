#include <string>
#include <iostream>

using namespace std;

bool matching(string a, string b, int b_offset)
{
    bool matching = true;
    if (a.size() <= b.size() - b_offset)
    {
        for (int i = 0; i < a.size(); i++)
        {
            if(a.at(i) != b.at(b_offset + i)){
                matching = false;
                break;
            }
        }
    } else {
        matching = false;
    }
    return matching;
}

int strstr(string needle, string haystack)
{
    int result = -1;
    for (int i = 0; i < haystack.size() - needle.size() + 1; i++)
    {
        if (matching(needle, haystack, i))
        {
            result = i;
            break;
        }
    }
    return result;
}

int main(int argc, char *argv[])
{
    string needle, haystack;
    printf("please enter the needle: \n");
    getline(cin, needle);
    printf("please enter the haystack: \n");
    getline(cin, haystack);
    int index = strstr(needle, haystack);
    printf("%i\n", index);
}