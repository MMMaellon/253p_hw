#include <string>
#include <iostream>
#include <vector>

using namespace std;

void updateCharacterMap(vector<int> &map, string addCharacters, string delCharacters = "")
{
    for (int i = 0; i < addCharacters.size(); i++)
    {
        map[addCharacters.at(i)] = map[addCharacters.at(i)] + 1;
    }
    for (int i = 0; i < delCharacters.size(); i++)
    {
        map[delCharacters.at(i)] = map[delCharacters.at(i)] - 1;
    }
}

bool matchingMaps(vector<int> &a, vector<int> &b)
{
    bool matching = a.size() == b.size();
    if (matching)
    {
        for (int i = 0; i < a.size(); i++)
        {
            if (a[i] != b[i])
            {
                matching = false;
                break;
            }
        }
    }
    return matching;
}

void emptyMap(vector<int> &map, int size)
{
    map.clear();
    for (int i = 0; i < size; i++)
    {
        map.push_back(0);
    }
}

vector<int> anagram(string needle, string haystack)
{
    vector<int> result;
    if (needle.size() <= haystack.size())
    {
        vector<int> needleMap;
        vector<int> hayWindow;
        emptyMap(needleMap, 256);
        emptyMap(hayWindow, 256);
        updateCharacterMap(needleMap, needle);
        updateCharacterMap(hayWindow, haystack.substr(0, needle.size()));

        for (int i = needle.size() - 1; i < haystack.size(); i++)
        {
            if (matchingMaps(needleMap, hayWindow))
            {
                result.push_back(i - (int)needle.size() + 1);
            }
            updateCharacterMap(hayWindow, string(1, haystack.at(i)), string(1, haystack.at(i - needle.size() + 1)));
        }
    }
    return result;
}

void printVector(vector<int> v)
{
    for (int i = 0; i < v.size(); i++)
    {
        if (i == 0)
        {
            printf("%i", v[i]);
        }
        else
        {
            printf(", %i", v[i]);
        }
    }

    if (v.size() == 0)
    {
        printf("Nothing to print");
    }
    printf("\n");
}

int main(int argc, char *argv[])
{
    string needle, haystack;
    printf("please enter the needle: \n");
    getline(cin, needle);
    printf("please enter the haystack: \n");
    getline(cin, haystack);
    vector<int> indexes = anagram(needle, haystack);
    printVector(indexes);
}