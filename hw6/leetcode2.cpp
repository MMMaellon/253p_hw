#include <vector>
#include <iostream>
#include <set>

using namespace std;

vector<int> eventualSafeNodes(vector<vector<int>> &graph, set<int> visited = set<int>(), int current = -1)
{
    set<int> safe;
    int oldSize = 0;
    do
    {
        oldSize = safe.size();
        for (int i = 0; i < graph.size(); i++)
        {
            bool isSafe = true;
            for (int neighbor : graph[i])
            {
                if (safe.count(neighbor) == 0)
                {
                    isSafe = false;
                }
            }
            if (isSafe)
            {
                safe.insert(i);
            }
        }
    } while (safe.size() > oldSize);

    return vector<int>(safe.begin(), safe.end());
}

int main(int argc, char *argv[])
{
    vector<vector<int>> testInput;
    testInput.push_back({1, 2});
    testInput.push_back({2, 3});
    testInput.push_back({5});
    testInput.push_back({0});
    testInput.push_back({5});
    testInput.push_back({});
    testInput.push_back({});
    printf("Answer: ");
    for (int ans : eventualSafeNodes(testInput))
    {
        printf("%i ", ans);
    }
    printf("\n");
}