#include <vector>
#include <iostream>

using namespace std;

int closedIsland(vector<vector<int>> &grid)
{
    int islands = 0;
    int id = 2;
    for (int i = 0; i < grid.size(); i++)
    {
        for (int j = 0; j < grid[i].size(); j++)
        {
            if (grid[i][j] == 0)
            {
                int groupID = 0;
                int top = 0;
                int left = 0;
                if (i > 0)
                {
                    top = grid[i - 1][j];
                }
                if (j > 0)
                {
                    left = grid[i][j - 1];
                }

                if (left == -1 || top == -1 || i == 0 || j == 0 || i == grid.size() - 1 || j == grid[i].size() - 1)
                {
                    groupID = -1;
                    if (left > 1 || top > 1)
                    {
                        islands--;
                        if (left > 1 && top > 1 && left != top)
                        {

                            islands--;
                        }
                    }

                    for (int leftIndex = 0; leftIndex < j && left > 1; leftIndex++)
                    {
                        if ((left > 1 && grid[i][leftIndex] == left) || (top > 1 && grid[i][leftIndex] == top))
                        {
                            grid[i][leftIndex] = groupID;
                        }
                    }

                    for (int topIndex = j; topIndex < grid[i].size() && i > 0 && top > 1; topIndex++)
                    {
                        if ((left > 1 && grid[i - 1][topIndex] == left) || (top > 1 && grid[i - 1][topIndex] == top))
                        {
                            grid[i - 1][topIndex] = groupID;
                        }
                    }
                }
                else if (left == 1 && top == 1)
                {
                    islands++;
                    groupID = id++;
                }
                else if (left == 1)
                {
                    groupID = top;
                }
                else if (top == 1 || top == left)
                {
                    groupID = left;
                }
                else
                {
                    groupID = left;
                    islands--;

                    for (int leftIndex = 0; leftIndex < j && left > 1; leftIndex++)
                    {
                        if ((left > 1 && grid[i][leftIndex] == left) || (top > 1 && grid[i][leftIndex] == top))
                        {
                            grid[i][leftIndex] = groupID;
                        }
                    }

                    for (int topIndex = j; topIndex < grid[i].size() && i > 0 && top > 1; topIndex++)
                    {
                        if ((left > 1 && grid[i - 1][topIndex] == left) || (top > 1 && grid[i - 1][topIndex] == top))
                        {
                            grid[i - 1][topIndex] = groupID;
                        }
                    }
                }
                grid[i][j] = groupID;
                // for (vector<int> row : grid)
                // {
                //     printf("\n");
                //     for (int box : row)
                //     {
                //         if (box == 1)
                //         {
                //             printf(" -- ");
                //         }
                //         else
                //         {
                //             printf(" %02i ", box);
                //         }
                //     }
                // }
                // printf("--\n\n");
                // printf("--\n\n");
            }
        }
    }
    return islands;
}

int main(int argc, char *argv[])
{
    vector<vector<int>> testInput;
    testInput.push_back({0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1});
    testInput.push_back({0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0});
    testInput.push_back({1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0});
    testInput.push_back({0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1});
    testInput.push_back({1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0});
    testInput.push_back({0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1});
    testInput.push_back({1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0});
    testInput.push_back({0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0});
    testInput.push_back({0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1});
    testInput.push_back({0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1});
    testInput.push_back({0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1});
    testInput.push_back({0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0});
    testInput.push_back({1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1});
    testInput.push_back({0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0});

    // testInput.push_back({-, -, 1, -, 1, -, -, -, 1, 1, -, -, -, 1, 1, -, 1, 1, -, 1});
    // testInput.push_back({-, 1, 1, -, -, -, -, 1, 0, 1, -, 1, 1, 1, 1, 1, 1, 1, -, -});
    // testInput.push_back({1, 1, -, -, -, -, -, -, 1, 0, 1, -, -, 1, -, -, -, 1, 1, -});
    // testInput.push_back({-, 1, -, 1, -, -, -, 1, 1, 1, -, -, -, -, -, -, 1, 1, -, 1});
    // testInput.push_back({1, 1, 1, -, -, -, 1, 1, 0, 0, 1, -, 1, 1, 1, -, -, 1, -, -});
    // testInput.push_back({-, 1, 1, 1, -, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, -, -, 1, 1});
    // testInput.push_back({1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, -, 1, 0, 0, 0, 1, -, -, -});
    // testInput.push_back({-, 1, 1, 0, 1, 1, -, -, 1, 1, 1, -, 1, 1, 1, 0, 1, -, 1, -});
    // testInput.push_back({-, -, 1, 1, -, 1, -, -, -, -, -, -, 1, -, 1, 1, -, -, 1, 1});
    // testInput.push_back({-, -, 1, -, -, -, -, 1, -, 1, 1, 1, 1, -, 1, 1, 1, -, 1, 1});
    // testInput.push_back({-, -, -, 1, -, 1, -, -, -, 1, 1, -, -, -, -, -, 1, 1, 1, 1});
    // testInput.push_back({-, -, -, -, 1, 1, -, -, -, -, -, -, 1, -, 1, 1, -, 1, -, -});
    // testInput.push_back({1, -, -, -, -, -, 1, 1, 1, -, -, 1, -, -, -, 1, -, -, 1, 1});
    // testInput.push_back({-, 1, 1, -, 1, 1, 1, -, 1, -, -, 1, -, -, -, -, 1, -, -, -});
    // testInput.push_back({1, 0, 1, 1, 1, 1, 0, 0, 1, 0});
    // testInput.push_back({1, 0, 1, 1, 0, 0, 0, 1, 1, 1});
    // testInput.push_back({0, 1, 1, 0, 0, 0, 1, 0, 0, 0});
    // testInput.push_back({1, 0, 1, 1, 0, 1, 0, 0, 1, 0});
    // testInput.push_back({0, 1, 1, 1, 0, 1, 0, 1, 0, 0});
    // testInput.push_back({1, 0, 0, 1, 0, 0, 1, 0, 0, 0});
    // testInput.push_back({1, 0, 1, 1, 1, 0, 0, 1, 1, 0});
    // testInput.push_back({1, 1, 0, 1, 1, 0, 1, 0, 1, 1});
    // testInput.push_back({0, 0, 1, 1, 1, 0, 1, 0, 1, 1});
    // testInput.push_back({1, 0, 0, 1, 1, 1, 1, 0, 1, 1});
    // testInput.push_back({0, 1, 1, 1, 1, 1, 1, 1});
    // testInput.push_back({1, 0, 1, 0, 0, 0, 0, 1});
    // testInput.push_back({1, 0, 1, 0, 0, 1, 0, 1});
    // testInput.push_back({1, 0, 0, 0, 0, 1, 0, 1});
    // testInput.push_back({1, 0, 0, 1, 0, 1, 0, 1});
    // testInput.push_back({1, 1, 0, 1, 0, 0, 0, 1});
    // testInput.push_back({0, 1, 1, 1, 1, 1, 1, 1});
    // testInput.push_back({1, 1, 0, 1, 1, 1, 1, 1, 1, 1});
    // testInput.push_back({0, 0, 1, 0, 0, 1, 0, 1, 1, 1});
    // testInput.push_back({1, 0, 1, 0, 0, 0, 1, 0, 1, 0});
    // testInput.push_back({1, 1, 1, 1, 1, 0, 0, 1, 0, 0});
    // testInput.push_back({1, 0, 1, 0, 1, 1, 1, 1, 1, 0});
    // testInput.push_back({0, 0, 0, 0, 1, 1, 0, 0, 0, 0});
    // testInput.push_back({1, 0, 1, 0, 0, 0, 0, 1, 1, 0});
    // testInput.push_back({1, 1, 0, 0, 1, 1, 0, 0, 0, 0});
    // testInput.push_back({0, 0, 0, 1, 1, 0, 1, 1, 1, 0});
    // testInput.push_back({1, 1, 0, 1, 0, 1, 0, 0, 1, 0});

    // testInput.push_back({0, 1, 1, 1, 1, 1, 1, 1});
    // testInput.push_back({1, 0, 1, 0, 0, 0, 0, 1});
    // testInput.push_back({1, 0, 1, 0, 0, 1, 0, 1});
    // testInput.push_back({1, 0, 0, 0, 0, 1, 0, 1});
    // testInput.push_back({1, 0, 0, 1, 0, 1, 0, 1});
    // testInput.push_back({1, 1, 0, 1, 0, 0, 0, 1});
    // testInput.push_back({1, 0, 0, 0, 0, 0, 0, 1});
    // testInput.push_back({0, 1, 1, 1, 1, 1, 1, 1});
    printf("Answer: %i\n", closedIsland(testInput));
}