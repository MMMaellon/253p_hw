#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <unordered_map>
#include <queue>
#include <limits>

using namespace std;

struct Node
{
    int x;
    int y;
    string name;
    int group;
    Node() : x(0), y(0), name(""), group(-1) {}
};

class Graph
{
private:
    vector<Node> nodes;
    priority_queue<pair<int, pair<int, int>>> distances;

    int collapseFind(int index, vector<Node> &n)
    {
        int groupID = index;
        vector<int> dirtyNodes;
        while (n[groupID].group != groupID)
        {
            dirtyNodes.push_back(groupID);
            groupID = n[groupID].group;
        }

        for (int j = 0; j < dirtyNodes.size(); j++)
        {
            n[dirtyNodes[j]].group = groupID;
        }
        return groupID;
    }

public:
    void addNode(Node node)
    {
        nodes.push_back(node);
    }

    void populateEdges()
    {
        for (int i = 0; i < nodes.size(); i++)
        {
            nodes[i].group = i;
            for (int j = 0; j < i; j++)
            {
                int xDistance = nodes[i].x - nodes[j].x;
                int yDistance = nodes[i].y - nodes[j].y;
                int distance = (xDistance * xDistance) + (yDistance * yDistance);
                // printf("EDGE: %s - %s | %i\n", nodes[i].name.c_str(), nodes[j].name.c_str(), distance);
                distances.push(make_pair(distance * -1, make_pair(i, j)));
            }
        }
    }

    void print()
    {
        printf("\n  |");
        // for (Node node : nodes)
        // {
        //     printf(" %s  ", node.name.c_str(), node.x, node.y);
        // }
        // for (int i = 0; i < nodes.size(); i++)
        // {
        //     printf("\n%s |", nodes[i].name.c_str());
        //     for (int j = 0; j < nodes.size(); j++)
        //     {
        //         if (i != j && edges[i][j] >= 0)
        //         {
        //             printf(" %02d ", edges[i][j]);
        //         }
        //         else
        //         {
        //             printf("    ");
        //         }
        //     }
        // }
        pair<int, pair<int, int>> longestEdge = distances.top();
        Node a = nodes[longestEdge.second.first];
        Node b = nodes[longestEdge.second.second];
        printf("Edge Count: %i\nShortest Edge: %s [%i, %i] - %s [%i, %i] (%i)\n", distances.size(), a.name.c_str(), a.x, a.y, b.name.c_str(), b.x, b.y, longestEdge.first);
    }

    unordered_map<int, vector<string>> cluster(int k)
    {
        unordered_map<int, vector<string>> clusters;
        priority_queue<pair<int, pair<int, int>>> d = distances;
        vector<Node> n = nodes;
        clusters.reserve(k);

        //create union/finds from spanning tree
        int i = n.size();
        while (i > k && !d.empty())
        {
            pair<int, int> edge = d.top().second;
            int groupA = collapseFind(edge.first, n);
            int groupB = collapseFind(edge.second, n);
            if (groupA != groupB)
            {
                n[groupA].group = groupB;
                i--;
            }
            d.pop();
        }

        //populate clusters from union finds
        for (int i = 0; i < n.size(); i++)
        {
            if (n[i].group == i)
            {
                vector<string> cluster;
                cluster.push_back(n[i].name);
                clusters.insert(make_pair(i, cluster));
            }
            else
            {
                int groupID = collapseFind(i, n);
                clusters[groupID].push_back(n[i].name);
            }
        }
        return clusters;
    }
};

int getNodeCount()
{
    printf("Please enter in the number of nodes\n");
    int nodeCount = 0;
    while (nodeCount <= 0)
    {
        cin >> nodeCount;
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        cin.clear();
    }
    return nodeCount;
}

Graph getGraph(int nodeCount)
{
    printf("Please enter each node in the following format: [name] [x position] [y position]\nEach node should be on its own line\n");
    int i = nodeCount;
    string line;
    stringstream ss;
    Graph graph;
    while (i > 0)
    {
        getline(cin, line);
        Node node;
        ss.clear();
        ss.str(line);
        ss >> node.name;
        ss >> node.x;
        ss >> node.y;
        if (ss.fail())
        {
            printf("Failed to parse node!\n");
        }
        else
        {
            i--;
            graph.addNode(node);
        }
    }
    graph.populateEdges();
    // graph.print();
    return graph;
}

int main(int argc, char *argv[])
{
    int nodeCount = getNodeCount();
    Graph graph = getGraph(nodeCount);
    int k = nodeCount;
    while (k > 0)
    {
        printf("Please enter a k value. Values of 0 or lower quit the program\n");
        cin >> k;
        if (k > nodeCount)
        {
            printf("Error! K must be lower than the total number of nodes (%i)\n", nodeCount);
        }
        else if (k > 0)
        {
            unordered_map<int, vector<string>> clusters = graph.cluster(k);
            int clusterCount = 0;
            for (pair<int, vector<string>> cluster : clusters)
            {
                printf("\nCluster %i: ", ++clusterCount);
                for (string node : cluster.second)
                {
                    printf("%s  ", node.c_str());
                }
            }
            printf("\n");
        }
    }
    printf("Goodbye!\n");
    return 0;
}